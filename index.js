const express = require("express");
const employees = require("./employees");

const app = express();
const PORT = 3000;

//Route Wellcome
app.get("/", function(req, res) {
  res.send("Welcome simple web app");
});

//Get all employees
app.get("/employees", function(req, res) {
  res.status(200).json({ employees: employees });
});

//Get single employee
app.get("/employees/:id", function(req, res) {
  const employee = employees.filter(
    employee => employee.id == parseInt(req.params.id)
  );
  res.status(200).json({ employee: employee });
});

app.listen(PORT, () => console.log(`Server is running on port ${PORT}!`));
