const employees = [
  {
    id: 1,
    firstName: "Lokesh",
    lastName: "Gupta",
    website: "howtodoinjava.com"
  },
  {
    id: 2,
    firstName: "Brian",
    lastName: "Schultz",
    website: "example.com"
  }
];
module.exports = employees;
